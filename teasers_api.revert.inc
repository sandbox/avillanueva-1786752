<?php

/**
 * @file
 * Revert a single teaser row
 */

/**
 * Revert a single field back to it's default settings.
 *
 * @param object $node
 *   A node object
 * @param string $module
 *   A module machine name string
 * @param string $field
 *   A field maching name string.
 */
function teasers_api_revert_teasers_single($node, $module, $field) {
  $field_data = field_get_items('node', $node, $field, $node->language);
  $callback_id = "teasers_build_{$field}";
  $teaser = module_invoke($module, $callback_id, $node, $field, $field_data);

  db_update('teasers')
    ->fields(array(
      'teaser' => $teaser['teaser'],
    ))
    ->condition('entity_id', $node->nid)
    ->condition('module', $module)
    ->condition('field', $field)
    ->execute();

  drupal_goto("node/$node->nid/teasers");
}
