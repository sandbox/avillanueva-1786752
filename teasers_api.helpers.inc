<?php

/**
 * @file
 * Helper functions used throughout the module
 */

/**
 * Serialize a set of arguments.
 *
 * @return string
 *   A serialized meta data array
 */
function teasers_api_set_meta_data($meta_data_array) {
  return serialize($meta_data_array);
}

/**
 * Serialize a set of arguments.
 *
 * @return mixed
 *   An unserialized meta data array
 */
function teasers_api_get_meta_data($meta) {
  return unserialize($meta);
}

/**
 * Get the field and data needed.
 *
 * @param object $node
 *   The node object
 *
 * @return array
 *   An array of teaser implementations
 */
function teasers_api_get_field_implimentations($node, $allow_override = TRUE) {
  $teasers = array();
  $field_registry = variable_get(TEASERS_FIELD_REGISTRY);

  foreach ($field_registry[$node->type] as $field => $label) {
    $implementations = array();
    $data = field_get_items('node', $node, $field, $node->language);
    $callback_id = "teasers_build_{$field}";

    if ($data === FALSE) {
      continue;
    }

    foreach (module_implements($callback_id) as $module) {
      $invoked_base = array(
        'node' => $node->nid,
        'module' => $module,
        'field' => $field,
      );

      $teaser = teasers_api_get_teaser_row($node, $module, $field);
      $to_merge = module_invoke($module, $callback_id, $node, $field, $data);

      if ($teaser !== FALSE && $allow_override == TRUE) {
        $to_merge['teaser'] = $teaser;
      }

      $implementations[] = array_merge($invoked_base, $to_merge);
    }

    $teasers[$field] = array(
      'label' => $label,
      'implementations' => $implementations,
    );
  }

  return $teasers;
}

/**
 * Get a row of information for a teaser.
 *
 * @param object $node
 *   The node object
 * @param string $module
 *   The module machine name.
 * @param string $field
 *   The field machine name.
 */
function teasers_api_get_teaser_row($node, $module, $field) {
  return db_select('teasers', 't')
    ->fields('t', array('teaser'))
    ->condition('t.entity_id', $node->nid)
    ->condition('t.module', $module)
    ->condition('t.field', $field)
    ->execute()
    ->fetchField();
}

/**
 * Save the teaser row to the database.
 *
 * @param object $node
 *   The node object
 * @param string $module
 *   The module machine name
 * @param string $field
 *   The field machine name
 * @param string $teaser
 *   The teaser text
 * @param string $status
 *   The status of the teaser if it's allowed or not.
 */
function teasers_api_save_teaser_row($node, $module, $field, $teaser, $status) {
  db_merge('teasers')
  ->key(array(
    'entity_id' => $node->nid,
    'module' => $module,
    'field' => $field,
  ))
  ->fields(array(
    'entity_id' => $node->nid,
    'module' => $module,
    'field' => $field,
    'teaser' => $teaser,
    'status' => $status,
  ))
  ->execute();
}

/**
 * Get a module's name by it's namespace.
 *
 * @param string $machine_name
 *   The module machine name
 *
 * @return string
 *   Module full name.
 */
function teasers_api_get_module_name($machine_name) {
  $sql = "SELECT * FROM {system} WHERE name=:name";
  $result = db_query($sql, array(':name' => $machine_name))->fetch();
  $info = unserialize($result->info);

  return $info['name'];
}

/**
 * Get the container field that will be used globally.
 *
 * @param string $field
 *   The field machine name
 *
 * @return string
 *   Container key
 */
function teasers_api_get_container_key($field) {
  return "container_{$field}";
}

/**
 * Only if the module exists in the database.
 *
 * @todo
 *   validate that the module exists
 *
 * @param string $module
 *   Module machine name
 *
 * @return string
 *   Module machine name
 */
function teasers_api_module_load($module) {
  return $module;
}

/**
 * Only if the field exists in the teasers database.
 *
 * @todo
 *   Validate that the field exists
 *
 * @param string $field
 *   The field machine name from the URL
 *
 * @return string
 *   The field machine name
 */
function teasers_api_field_load($field) {
  return $field;
}

/**
 * Add the teaser variables to the node to be used in the theme.
 *
 * @param object $node
 *   A node object
 * @param array &$variables
 *   The node's variables to be used in the theme/template
 */
function teasers_api_generate_variables($node, &$variables) {
  foreach (teasers_api_get_field_implimentations($node) as $data) {
    foreach ($data['implementations'] as $implementation) {
      $variable_id = "{$implementation['module']}_{$implementation['field']}";
      $variables[$variable_id] = $implementation['teaser'];
    }
  }
}

/**
 * Helper function to get the default data for the settings form.
 *
 * @param array $instance
 *   The teaser instance array
 *
 * @return array
 *   The modfified data array
 */
function teasers_api_get_default_instance_data($instance) {
  $data = NULL;
  if (isset($instance['widget']['settings']['teasers'])) {
    $data = $instance['widget']['settings']['teasers'];
  }

  return $data;
}

/**
 * Settings overrides for the form if a hook demands it.
 *
 * @param array $f
 *   Form array
 * @param string $container
 *   The fieldset machine name
 * @param int $key
 *   The item in the array
 * @param string $hook
 *   The hook's settings
 */
function teasers_api_node_teaser_form_settings(&$f, $container, $key, $hook) {
  if (!empty($hook['maxlength'])) {
    $f[$container][$key]['teaser']['#maxlength'] = $hook['maxlength'];
    $f[$container][$key]['teaser']['#maxlength_js'] = TRUE;
  }

  // Hardcoded format ID for now.
  if (!empty($hook['wysiwyg'])) {
    $f[$container][$key]['teaser']['#type'] = 'text_format';
    $f[$container][$key]['teaser']['#format'] = 2;
  }

  if (!empty($hook['disabled'])) {
    // Stub conditional.
  }
}

/**
 * Rebuild the allowed field registry for all node types.
 *
 * Also stores it in the variables table for speed and minimalizing
 * code duplication.
 */
function teasers_api_rebuild_field_registry() {
  $field_data = array();

  foreach (field_info_instances('node') as $type => $fields) {
    foreach ($fields as $field => $info) {
      if (!isset($info['widget']['settings']['teasers']) ||
          $info['widget']['settings']['teasers'] !== 1) {
        continue;
      }

      $field_data[$type][$field] = $info['label'];
    }
  }

  variable_set(TEASERS_FIELD_REGISTRY, $field_data);
}
