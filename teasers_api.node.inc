<?php

/**
 * @file
 * Node teaser functions
 */

/**
 * Node/teaser callback.
 *
 * @param object $node
 *   The node array
 *
 * @return array
 *   A build form array
 */
function teasers_api_local_node_callback($node) {
  $teasers = teasers_api_get_field_implimentations($node);
  $form_id = 'teasers_api_node_implimentations_form';
  return drupal_get_form($form_id, $teasers, $node);
}

/**
 * Create the form that will be used to draw the teasers page to the screen.
 *
 * @param srray $form
 *   An unbuilt
 * @param srray $s
 *   A form state array
 * @param srray $teasers
 *   A set of teaser implimentations
 *
 * @return array
 *   A form array
 */
function teasers_api_node_implimentations_form($form, &$s, $teasers, $node) {
  foreach ($teasers as $field => $data) {
    $container = teasers_api_get_container_key($field);
    $title = 'Field: ' . $data['label'];
    $form['#node'] = $node;

    $form[$container] = array(
      '#title' => t("@title", array('@title' => $title)),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      '#theme' => 'form_table_theme',
    );

    foreach ($data['implementations'] as $key => $teaser) {
      $meta_data = array(
        'nid' => $node->nid,
        'module' => $teaser['module'],
        'field' => $field,
        'teaser' => $teaser['teaser'],
      );

      $form[$container][$key] = array(
        'exclude' => array(
          '#type' => 'checkbox',
          '#default_value' => 0,
        ),
        'meta_data' => array(
          '#type' => 'hidden',
          '#value' => teasers_api_set_meta_data($meta_data),
        ),
        'module' => array(
          '#markup' => teasers_api_get_module_name($teaser['module']),
        ),
        'teaser' => array(
          '#type' => 'textarea',
          '#cols' => 20,
          '#default_value' => $teaser['teaser'],
        ),
        'revert' => array(
          '#markup' => teasers_api_form_build_revert_button($teaser),
        ),
      );

      // Do the form overrides here.
      teasers_api_node_teaser_form_settings($form, $container, $key, $teaser);
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Teasers'),
    '#submit' => array('teasers_api_local_node_callback_form_submit'),
  );

  $form['revert'] = array(
    '#type' => 'submit',
    '#value' => t('Revert All'),
    '#submit' => array('teasers_api_node_implimentations_form_revert'),
  );

  return $form;
}


/**
 * Submit callback.
 *
 * @param array $form
 *   A form array
 * @param array $state
 *   A state array
 */
function teasers_api_local_node_callback_form_submit($form, &$state) {
  $field_registry = variable_get(TEASERS_FIELD_REGISTRY);
  $input = $state['input'];
  $node = $form['#node'];

  foreach ($field_registry[$node->type] as $field_name => $label) {
    $container_id = teasers_api_get_container_key($field_name);
    if (!isset($input[$container_id])) {
      continue;
    }

    foreach ($input[$container_id] as $teaser) {
      $meta_data = teasers_api_get_meta_data($teaser['meta_data']);
      teasers_api_save_teaser_row(
        $node,
        $meta_data['module'],
        $meta_data['field'],
        $teaser['teaser'],
        $teaser['exclude']
      );
    }
  }
}

/**
 * Revert all teaser implimentations.
 *
 * @param array $form
 *   A form array
 * @param array $state
 *   A state array
 */
function teasers_api_node_implimentations_form_revert($form, &$state) {
  $node = $form['#node'];
  $teasers = teasers_api_get_field_implimentations($node, FALSE);

  foreach ($teasers as $data) {
    foreach ($data['implementations'] as $key => $implementation) {
      teasers_api_save_teaser_row(
        $node,
        $implementation['module'],
        $implementation['field'],
        $implementation['teaser'],
        1
      );
    }
  }
}

/**
 * Helper function to build the revert button link.
 *
 * @param array $implementation
 *   An array of a teaser implementation
 *
 * @return string
 *   A link string
 */
function teasers_api_form_build_revert_button($implementation) {
  $module = $implementation['module'];
  $field = $implementation['field'];
  $node = $implementation['node'];
  $link = "admin/teasers/revert/$node/$module/$field";

  $attr = array(
    'attributes' => array(
      'class' => 'button',
    ),
  );

  return l(t('Revert'), $link, $attr);
}
