<?php

/**
 * @file
 * Container for all theme functions
 */

/**
 * Theme a table out of a set of field implimentations (elements).
 *
 * @param array $vars
 *   An array of theme variables
 *
 * @return string
 *   A rendered table row
 */
function theme_form_table_theme($vars) {
  $rows = array();
  $element = $vars['element'];

  $header = array(
    'exclude' => t('Exclude'),
    'module' => t('Module'),
    'teaser' => t('Teaser'),
    'revert' => t('Revert'),
    'meta_data' => t(''),
  );

  foreach (element_children($element) as $key) {
    $row = array();

    $row['data'] = array();
    foreach ($header as $fieldname => $title) {
      $row['data'][] = drupal_render($element[$key][$fieldname]);
    }

    $rows[] = $row;
  }

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
  ));
}
