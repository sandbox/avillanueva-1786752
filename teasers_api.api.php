<?php

/**
 * @file
 * API DOCUMENATION -------------------------------------
 *
 * The API is broken down into 3 types of hooks and vary depending on their
 * file. These functions also get more spcific by node type (if exists). Reason
 * for this is so that a module can target a field spcifically to a type of node
 * and not build teaser text globally for all of FIELDNAME. This allows for a
 * level of complexitiy that may or may not be needed by most module developers.
 *
 * HOOKS ------------------------------------------------
 * The hooks are build, validate and save.
 *    - hook_teasers_build_FIELDNAME
 *    - hook_teasers_build_NODETYPE_FIELDNAME
 *
 *    - hook_teasers_validate_FIELDNAME
 *    - hook_teasers_validate_NODETYPE_FIELDNAME
 *
 *    - hook_teasers_save_FIELDNAME
 *    - hook_teasers_save_NODETYPE_FIELDNAME
 *
 * BUILD FUNCTIONS
 * The build functions allow modules to hook into the teaser API and output a
 * truncated text for users to see and possibly override.
 *
 * VALIDATE FUNCTIONS
 * Validation functions allow the module developers to validate text to make
 * sure that the text being saved is of maximum quality
 *
 * SAVE FUNCTIONS
 * If a module developer needs to change the text before being saved their
 * custom save function will be called. This function must return a true to
 * notify that it is doing it's own work otherwise the teaser API will go ahead
 * and run it's own save functionality.
 *
 * FILES ------------------------------------------------
 * Hooks can be seperated into their own files to keep their code tidy. To do
 * this there are two file patterns that a module developer can use. Either a
 * field file or a node file. A field file is generic file just for fields.
 *
 * A node type file is for all fields that a module wants to maintain for all
 * it's fields. A module can have many node type files but only a single field
 * file.
 *
 * The include files can be anywhere in the module directory as the Teasers API
 * will find it.
 *
 * Naming conventions for teaser include files are are as follows:
 *    - module.teasers.inc => field file include
 *    - module.node.type1.inc
 *    - module.node.type2.inc
 *    - module.node.type3.inc
 */

/**
 * Build our your teaser data. Format: hook_teasers_build_{FIELD_NAME}.
 *
 * @todo Allow to be more spcific by adding node type
 *    - teasers_build_{NODE_TYPE}_{$field_name}
 *
 * @param object $node
 *   The node objects
 * @param string $field
 *   The field name
 * @param string $data
 *   The field data
 *
 * @return array
 *   The implimentation array including the teaser text and the settings.
 */
function hook_teasers_build_FIELDNAME($node, $field, $data) {
  // Wysiwyg setting. Not convinced if this is even a good idea.
  return array(
    'teaser' => 'text',
    'maxlength' => 10,
  );
}

/**
 * Build our your teaser data using a more spcific hook format.
 *
 * Follows this format: hook_teasers_build_{FIELD_NAME}
 *
 * @todo Allow to be more spcific by adding node type
 *    - teasers_build_{NODE_TYPE}_{$field_name}
 *
 * @param object $node
 *   The node objects
 * @param string $field
 *   The field name
 * @param string $data
 *   The field data
 *
 * @return array
 *   The implimentation array including the teaser text and the settings.
 */
function hook_teasers_build_NODETYPE_FIELDNAME($node, $field, $data) {
  // Wysiwyg setting. Not convinced if this is even a good idea.
  return array(
    'teaser' => 'text',
    'maxlength' => 10,
  );
}

/**
 * Allow validation services for a fieldname.
 *
 * @todo Allow to be more spcific by adding node type
 *    - teasers_build_{NODE_TYPE}_{$field_name}
 *
 * @param object $node
 *   The node objects
 * @param string $field
 *   The field name
 * @param string $data
 *   The field data
 *
 * @return array
 *   The implimentation array including the teaser text and the settings.
 */
function hook_teasers_validate_FIELDNAME($node, $field, $data) {

}

/**
 * Allow validation services for a fieldname specifically by node type.
 *
 * @todo Allow to be more spcific by adding node type
 *    - teasers_build_{NODE_TYPE}_{$field_name}
 *
 * @param object $node
 *   The node objects
 * @param string $field
 *   The field name
 * @param string $data
 *   The field data
 *
 * @return array
 *   The implimentation array including the teaser text and the settings.
 */
function hook_teasers_validate_NODETYPE_FIELDNAME($node, $field, $data) {
}

/**
 * Allow modification before saving the text to the database.
 *
 * @todo Allow to be more spcific by adding node type
 *    - teasers_build_{NODE_TYPE}_{$field_name}
 *
 * @param object $node
 *   The node objects
 * @param string $field
 *   The field name
 * @param string $data
 *   The field data
 *
 * @return array
 *   The implimentation array including the teaser text and the settings.
 */
function hook_teasers_save_FIELDNAME($node, $field, $data) {
  return TRUE;
}

/**
 * Allow mutation before saving the text to the database specific to node type.
 *
 * @todo Allow to be more spcific by adding node type
 *    - teasers_build_{NODE_TYPE}_{$field_name}
 *
 * @param object $node
 *   The node objects
 * @param string $field
 *   The field name
 * @param string $data
 *   The field data
 *
 * @return array
 *   The implimentation array including the teaser text and the settings.
 */
function hook_teasers_save_NODETYPE_FIELDNAME($node, $field, $data) {
  return TRUE;
}
